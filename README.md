[![Build Status](https://gitlab.com/schrieveslaach/nextcloud-spgverein-app/badges/master/build.svg)](https://gitlab.com/schrieveslaach/nextcloud-spgverein-app/pipelines)
[![Download](https://img.shields.io/badge/download-spgverein.tar.gz-blue.svg)](https://gitlab.com/schrieveslaach/nextcloud-spgverein-app/-/jobs/artifacts/master/raw/spgverein.tar.gz?job=package)

# SPG Verein

[SPG-Verein](https://spg-direkt.de/) is a windows program for managing club members. For example, this program supports a club to generate direct debits. The nextcloud app *spgverein* implements additional behaviour for the club. 

- [x] Web-Interface to view all members of the club
- [x] Download address lists as PDF to print them on labels
- [ ] Provide all members as contacts in the addressbook
- [ ] Provide all member birth dates in the calendar

## Screenshots

![Screenshot SPG Verein](assets/screenshot-01.png)

![Screenshot SPG Verein](assets/screenshot-02.png)

## Installation

[Download](https://gitlab.com/schrieveslaach/nextcloud-spgverein-app/-/jobs/artifacts/master/raw/spgverein.tar.gz?job=package) and place this app in `nextcloud/apps/` and extract with `tar xvf`.


