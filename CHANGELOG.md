# 0.7.0

- Update JS Dependencies

# 0.6.0

- Improve UI: align it with Nextcloud design
- Show that there is no data aivalable to display.

# 0.5.1

- Fix app signing (#2)

# 0.5.0

- Parse `mitgl.dat` file as members
- Group members by related member id
- Show members in web interface sorted by districts
- Download members as CSV

